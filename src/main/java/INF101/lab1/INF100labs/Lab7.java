package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 { //cake
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
                
    ArrayList<ArrayList<Integer>> grid0 = new ArrayList<>();
    grid0.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
    grid0.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
    grid0.add(new ArrayList<>(Arrays.asList(31, 32, 33)));
    removeRow(grid0, 0);
    for (int i = 0; i < grid0.size(); i++) {
        System.out.println(grid0.get(i));
    }
    

    ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
    grid1.add(new ArrayList<>(Arrays.asList(3, 0, 9)));
    grid1.add(new ArrayList<>(Arrays.asList(4, 5, 3)));
    grid1.add(new ArrayList<>(Arrays.asList(6, 8, 1)));

    boolean equalSums1 = allRowsAndColsAreEqualSum(grid1);
    System.out.println(equalSums1); // false


    ArrayList<ArrayList<Integer>> grid2 = new ArrayList<>();
    grid2.add(new ArrayList<>(Arrays.asList(3, 4, 6)));
    grid2.add(new ArrayList<>(Arrays.asList(0, 5, 8)));
    grid2.add(new ArrayList<>(Arrays.asList(9, 3, 1)));

    boolean equalSums2 = allRowsAndColsAreEqualSum(grid2);
    System.out.println(equalSums2); // false

    ArrayList<ArrayList<Integer>> grid3 = new ArrayList<>();
    grid3.add(new ArrayList<>(Arrays.asList(1, 2, 3, 4)));
    grid3.add(new ArrayList<>(Arrays.asList(2, 3, 4, 1)));
    grid3.add(new ArrayList<>(Arrays.asList(3, 4, 1, 2)));
    grid3.add(new ArrayList<>(Arrays.asList(4, 1, 2, 3)));

    boolean equalSums3 = allRowsAndColsAreEqualSum(grid3);
    System.out.println(equalSums3); // true

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        //throw new UnsupportedOperationException("Not implemented yet.");
            
        for (int i = 0; i < grid.size(); i++) {
            if (i == row) {
                grid.remove(row);

            } else {System.out.println(grid.get(i));
            }
    }
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        if (grid == null || grid.isEmpty()) {
            return true; // Tom liste antas å være matchende
        }

        // Sorterer hver indre liste
        List<Integer> førsteListe = new ArrayList<>(grid.get(0));
        Collections.sort(førsteListe);

        for (int i = 1; i < grid.size(); i++) {
            List<Integer> nesteListe = new ArrayList<>(grid.get(i));
            Collections.sort(nesteListe);
            if (!førsteListe.equals(nesteListe)) {
                return false; // Fant en liste som ikke matcher
            }
        }
        return true; // Alle listene matcher
    }

    }

