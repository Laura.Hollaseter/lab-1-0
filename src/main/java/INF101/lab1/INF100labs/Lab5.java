package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        ArrayList<Integer> multipliedList = multipliedWithTwo(list);
        System.out.println(multipliedList);

        ArrayList<Integer> list_1 = new ArrayList<>(Arrays.asList(3, 3, 1, 3, 2, 4, 3, 3, 3));
        ArrayList<Integer> removedList = removeThrees(list_1);
        System.out.println(removedList);

        ArrayList<Integer> list_2 = new ArrayList<>(Arrays.asList(1, 1, 2, 1, 3, 3, 3, 2));
        ArrayList<Integer> uniqueList = uniqueValues(list_2);
        System.out.println(uniqueList);

        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> b = new ArrayList<>(Arrays.asList(4, 2, -3));
        addList(a, b);
        System.out.println(a);

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        ArrayList<Integer> multipliedList = new ArrayList<>();
        for (int number : list) {
            multipliedList.add(number * 2);
        }
        return multipliedList;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        ArrayList<Integer> removedList = new ArrayList<>();
        for (int number : list) {
            if (number != 3) {
                removedList.add(number);
            }
        }
        return removedList;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        ArrayList<Integer> uniqueList = new ArrayList<>();
        for (int number : list) {
            if (!uniqueList.contains(number)) {
                uniqueList.add(number);
            }
        }
        return uniqueList;
    
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        for (int i = 0; i < a.size(); i++) {
            a.set(i, a.get(i) + b.get(i));
        }
    }

}