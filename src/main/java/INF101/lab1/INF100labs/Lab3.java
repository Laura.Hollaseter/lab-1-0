package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        //multiplesOfSevenUpTo(49);

        //multiplicationTable(5);

        crossSum(123);

    }

    public static void multiplesOfSevenUpTo(int n) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        ArrayList<Integer> temp_2 = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            if (i % 7 == 0) {
                temp_2.add(i);
            }
        }
        for (int num : temp_2) {
            System.out.println(num);
        }
    }

    public static void multiplicationTable(int n) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        for (int i = 1; i <= n; i++) {
            System.out.print(i + ":");
            for (int j = 1; j <= n; j++) {
                System.out.print(" " + (i * j));
            }
            System.out.println();
        }
    }

    public static int crossSum(int num) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        int sum = 0;
        while (num > 0) {
            sum += num % 10;  // Legger til siste siffer
            num /= 10;        // Fjerner siste siffer
        }
        return sum;
    }
    }

