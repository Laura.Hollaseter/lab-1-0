package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("Game", "Action", "Champion");
        System.out.println(isLeapYear(2020));
        System.out.println(isLeapYear(2021));
        System.out.println(isEvenPositiveInt(123456));
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        String[] words = {word1, word2, word3};
        int maxLength = 0;

        // Finn maksimal lengde
        for (String word : words) {
            if (word.length() > maxLength) {
                maxLength = word.length();
            }
        }

        // Skriv ut ord med maksimal lengde
        for (String word : words) {
            if (word.length() == maxLength) {
                System.out.println(word);
            }
        }
    }

    public static boolean isLeapYear(int year) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        if (year % 4 != 0) {
            return false;
        } else if (year % 100 != 0) {
            return true;
        } else {
            return year % 400 == 0;
        }
    }

    public static boolean isEvenPositiveInt(int num) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        return num >= 0 && num % 2 == 0;
    }

}
