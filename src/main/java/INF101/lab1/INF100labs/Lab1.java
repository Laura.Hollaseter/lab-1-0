package INF101.lab1.INF100labs;

import java.util.Scanner;

/**
 * Implement the methods task1, and task2.
 * These programming tasks was part of lab1 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/1/
 */
public class Lab1 {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        task1();
        
        task2();
    }
    public static void task1() {
        //throw new UnsupportedOperationException("Not implemented yet.");
        System.out.println("Hei, det er meg, datamaskinen.");
        System.out.println("Hyggelig å se deg her.");
        System.out.println("Lykke til med INF101!");
    }
    public static void task2() {        
        //throw new UnsupportedOperationException("Not implemented yet.");
        System.out.println("Hva er ditt navn? ");
        String navn = sc.nextLine();
        System.out.println("Hva er din adresse? ");
        String addy = sc.nextLine();
        System.out.println("Hva er ditt postnummer og poststed? ");
        String sted = sc.nextLine();
        System.out.println(navn + "s adresse er: ");
        System.out.println();
        System.out.println(navn);
        System.out.println(addy);
        System.out.println(sted);
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public static String readInput(String prompt) {
        System.out.println(prompt);
        sc = new Scanner(System.in);
        String userInput = sc.nextLine();
        return userInput;
    }

}
