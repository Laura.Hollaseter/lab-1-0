package INF101.lab1.rockPaperScissors;

import java.util.List;
import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;
//kake

public class RockPaperScissors {
	
    public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundNumber = 1;
    int humanScore = 0;
    int computerScore = 0;
    //List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    String[] choices = {"rock", "paper", "scissors"};
       
    public void run() {
      while(true) {
            System.out.println("Let's play round "+ roundNumber);
            String human_choice=user_choice();
            String  computer_choice=random_choice();
            String choice_string="Human chose "+ human_choice+", computer chose "+ computer_choice;
 
             if(isWinner(human_choice.toLowerCase(), computer_choice.toLowerCase())) {
                  System.out.println(choice_string+" Human wins.");
                  humanScore+=1;
                  
            }
            else if(isWinner(computer_choice,human_choice)){
                  System.out.println(choice_string+" Computer wins.");
                  computerScore+=1;
            }
            else {
                  System.out.println(choice_string+" It's a tie.");
            }
            System.out.println("Score: human "+ humanScore+", computer "+ computerScore);
            
            String continue_answer=continue_playing();
            if(continue_answer.equals("n")) {
                  break;
            }
      }
      System.out.println("Bye bye :)");
    }

    public String random_choice(){
        Random dice = new Random();
        int number;
        number = dice.nextInt(3);
        
        return choices[number];
    }
    public Boolean isWinner(String choice1, String choice2){
        if(choice1.equals("paper"))
        {return choice2.equals("rock");}
    else if (choice1.equals("scissors"))
        {return choice2.equals("paper");}
    else
        {return choice2.equals("scissors");}
      
 
 
    }
    

    // public String user_choice() {
    //     String human_choice;
    //     do {
    //         System.out.println("Your choice (Rock/Paper/Scissors)?");
    //         human_choice = sc.nextLine();
    //     } while (!validate_input(human_choice.toLowerCase(), choices));
    //     return human_choice;
    // }
    
    public String user_choice() {
        String human_choice;
        do {
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            human_choice = sc.nextLine();
            if (!validate_input(human_choice.toLowerCase(), choices)) {
                System.out.println("I do not understand " + human_choice + ". Could you try again?");
                human_choice = ""; // Reset the choice to force the loop to continue
            }
        } while (human_choice.isEmpty());
        return human_choice;
    }
    


    // public String continue_playing() {
    //     String continue_answer;
    //     do {
    //         System.out.println("Do you wish to continue playing? (y/n)?");
    //         continue_answer = sc.nextLine().toLowerCase();
    //     } while (!validate_input(continue_answer, new String[] {"y", "n"}));
    //     return continue_answer;
    // }
    
    public String continue_playing() {
        String continue_answer;
        do {
            System.out.println("Do you wish to continue playing? (y/n)?");
            continue_answer = sc.nextLine().toLowerCase();
            if (!validate_input(continue_answer, new String[]{"y", "n"})) {
                System.out.println("I do not understand " + continue_answer + ". Could you try again?");
                continue_answer = ""; // Reset the answer to force the loop to continue
            }
        } while (continue_answer.isEmpty());
        return continue_answer;
    }

    public boolean validate_input(String s,String[] choices) {
      
      //check whether array 'choices' contains string 's'
      for(String e:choices) {
            if(e.equals(s)) {
                  return true;
            }
      }
      
      
            return false;
      
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}